/* O Buffer é uma representação na memória do computador 
usado para transitar dados em alta velocidade, em forma binária */

const buffer = Buffer.from("Tudo bem")

// HEX
console.log(buffer)
//DEC
console.log(buffer.toJSON())