
/*
IMPORTAÇAO VIA STREAM

1GB - 1.000.000+ de registros
POST /upload import.csv - Como fazemos hoje

10mb/s - 100s -> insercoes no banco de dados


A cada 10 mb enviados de arquivos é possivel pegar 10.000 linhas
Porque já não ir inserindo essa quantidade ao inves de esperar todo o arquivo
carregar para dai sim enviar?

Envio deve acontecer enquanto é processado.
Readble Streams / Writeable Stream
Lendo ados poucos/ escrevendo aos poucos


No Node toda porta de entrada e saída é automaticamente uma Stream! ✅
stdin e stdout são os processos utilizados

*/

// process.stdin
//   .pipe(process.stdout)

import { Readable, Transform, Writable } from 'node:stream';

class OneToHundredStream extends Readable {

  index = 1
  _read(){
    const i = this.index++

   setTimeout(() => {
    if(i > 100){
      this.push(null);
    }else {
      const buf = Buffer.from(String(i))
      this.push(buf)
    }
   }, 1000)
  }
}

class InversedNumberStream extends Transform{
  _transform(chunck, encoding, callback){
    const transformed = Number(chunck.toString()) * -1

    callback(null, Buffer.from(String(transformed)))
  }
}

class MultiplyByTenStreams extends Writable {
  _write(chunck, encoding, callback){
    console.log(Number(chunck.toString()) * 10)

    callback()
  }
}


new OneToHundredStream()
  // .pipe(new InversedNumberStream())
  // .pipe(new MultiplyByTenStreams())
