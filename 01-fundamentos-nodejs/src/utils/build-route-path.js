export function BuildRoutePath(path) {
  const routeParametersRegex = /:([a-zA-Z]+)/g
  const pathWithParams = path.replaceAll(routeParametersRegex, '(?<$1>[a-z0-9\-_]+)')

  console.log(Array.from(path.matchAll(routeParametersRegex)))
  const pathRegex = new RegExp(`ˆ${pathWithParams}`)

  return pathRegex
}