import { randomUUID } from 'node:crypto';
import { Database } from './database/database.js';
import { BuildRoutePath } from './utils/build-route-path.js';

const database = new Database();
export const routes = [
  {
    method: 'GET',
    path: BuildRoutePath('/users'),
    handler: (request, response) => {
      const users = database.select('users')
      return response
        .end(JSON.stringify(users));
    }
  },
  {
    method: 'POST',
    path: BuildRoutePath('/users'),
    handler: (request, response) => {
      const { name, email } = request.body

      const user = {
        id: randomUUID(),
        name,
        email,
      }

      database.insert('users', user);

      return response.writeHead(201).end();
    }
  },
  {
    method: 'DELETE',
    path: BuildRoutePath(`users/:id`),
    handler: (request, response) => {
      con
    }
  }
]